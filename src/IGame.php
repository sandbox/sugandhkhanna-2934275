<?php

namespace Drupal\tic_tac_toe;

/**
 * Interface for Game class 
 */
interface IGame {
	/**
	 * Constructor 
	 * @param array $config
	 */
	public function __construct(array $config); 
	/**
	 * Returns JSON object
	 */
	public function __toString();
} 
