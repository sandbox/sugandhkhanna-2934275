<?php

namespace Drupal\tic_tac_toe;

/**
 * Min Max Interface 
 */
Interface IMinMax { 
	/**
	 * Constructor 
	 * @param Game $game
	 */
	public function __construct(Game $game);
} 