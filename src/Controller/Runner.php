<?php

namespace Drupal\tic_tac_toe\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\tic_tac_toe\TicTacToe;
use Drupal\tic_tac_toe\Game;

/**
 * Defines a route for device model entity autocomplete form elements.
 */
class Runner extends ControllerBase  {
  public function ajax_runner_callback(Request $request) {

  	$pos1 = $request->request->get('pos1');
  	$pos2 = $request->request->get('pos2');
  	$gameBoard = $request->request->get('gameBoard');


  	if (!isset($pos1) || !isset($pos2)){
		die("please try to play the game..");
	  } 

	  $config = array(
			'player'	=>	Game::PLAYER,
			'move'		=>	array($pos1,$pos2),
		); 

    //if we starting new game we don't realy have to send game board 
		if (isset($gameBoard)){
			$config['gameBoard'] = $gameBoard;
		}
		$ticTacToe = new TicTacToe($config);
		$ticTacToe = (string) $ticTacToe;
		echo $ticTacToe;
		die;
	}
}


