<?php

namespace Drupal\tic_tac_toe\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form field for searching node items.
 */
class TicTacToeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tic_tac_toe';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'tic_tac_toe/tic_tac_toe';
    
    $form['#theme'] = 'tic_tac_toe';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  
  }

}
